#include <com/Com.h>
#include "App.h"

Define_Module (Com);

int Com::numInitStages() const
{
	//ensuring com is the last setup module.
	int stages = 3;
	return stages;
}


void Com::initialize(int stage)
{
	if(stage == 2)
	{
		// Store this node eid
		this->eid_ = this->getParentModule()->getIndex();

		// Store packetLoss probability
		this->packetLoss_ = par("packetLoss").doubleValue();

		int nodesNumber = this->getParentModule()->getParentModule()->par("nodesNumber");
		int satNodesNumber = this->getParentModule()->getParentModule()->par("satNodesNumber");

		//Check medium access control layer type
		string macString = par("mac");
		//TODO WHY DO WE HAVE A COM MODULE FOR EID=0????? Ignoring for now (maybe this is the central node or something)
		if(this->eid_ > 0)
		{
			if (macString.compare("None") == 0)
			{
				//basic MAC with infinite timeslots (e.g. no tdma) - results are identical to original simulation without modification
				mac = new MAC();

			}
			else if (macString.compare("TDMA Centralized") == 0)
			{
				//Note
				//TODO: Make use of the satellite nodes# (excluding GS)
				//TODO: This might be useful if we want satellites to act different from GS, e.g. maybe GS have more antennas/freq bands/etc.

				double guardBandPercent = par("guardBandPercentage").doubleValue();
				mac = new MACTDMACentralized(this->eid_,satNodesNumber,guardBandPercent,timeSlotPlan_);

			}
			else if (macString.compare("TDMA Decentralized") == 0)
			{
				//TODO: implement decentralized timeslot requests etc
				//mac = new MACTDMADecentralized();
			}
			else
			{
				cout << "dtnsim error: unknown mac layer type: " << macString << endl;
				exit(1);
			}

			//schedule the first timeslot (we must query the mac to know what this is.)
			TimeslotMsg *timeslotMsgStart = new TimeslotMsg("timeslotStart", TIMESLOT_START_TIMER);
			timeslotMsgStart->setSchedulingPriority(TIMESLOT_START_TIMER);
			//schedule now
			simtime_t firstSlot = mac->getFirstSlotTime();
			if(firstSlot == SIMTIME_MAX)
			{
				cout << "WARNING: Node Eid " << this->eid_ << " never has a timeslot!" << endl;
				//For now just place the first slot time of this beyond the actual sim time limit (84600 sec) to prevent errors with simtime_max
				//This scheduled slot should never actually happen though.
				firstSlot = 86500;
			}
			scheduleAt(mac->getFirstSlotTime(), timeslotMsgStart);


			//statistics
			comBundlesQueued = registerSignal("comBundlesQueued");
			comBundlesSent = registerSignal("comBundlesSent");
		}

	}
}

void Com::setContactTopology(ContactPlan &contactTopology)
{
	this->contactTopology_ = contactTopology;
}

void Com::setTimeSlotPlan(std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> timeSlotPlan)
{
	this->timeSlotPlan_ = timeSlotPlan;
}

void Com::handleMessage(cMessage *msg)
{
	if (msg->getKind() == BUNDLE || msg->getKind() == BUNDLE_CUSTODY_REPORT)
	{
		BundlePkt* bundle = check_and_cast<BundlePkt *>(msg);

		if (eid_ == bundle->getNextHopEid())
		{

			// This is an inbound message, check if packet was lost on the way
			if (packetLoss_ > uniform(0, 1.0))
			{
				// Packet was lost in the way, delete it
				cout << simTime() << " Node " << eid_ << " Bundle id " << bundle->getBundleId() << " lost on the way!" << endl;
				delete bundle;
			}
			else
			{
				// received correctly, send to Dtn layer
				send(msg, "gateToDtn$o");
			}
		}
		else
		{
			//TODO: Might want special functionality if this node is a ground station. E.g. ground stations might have more resources,
			//Maybe they have multiple antennas/frequencies and we can ignore timeslotting and send immediately.
			//For now, assume everyone is using timeslotting.

			//emit(comBundlesQueued,true);

			cModule *destinationModule = this->getParentModule()->getParentModule()->getSubmodule("node", bundle->getNextHopEid())->getSubmodule("com");
			double linkDelay = contactTopology_.getRangeBySrcDst(eid_, bundle->getNextHopEid());
			if (linkDelay == -1)
			{
				//cout << "warning, range not available for nodes " << eid_ << "-" << bundle->getNextHopEid() << ", assuming range is 0" << endl;
				linkDelay = 0;
			}
			double txDuration = (double) bundle->getByteLength() / bundle->getDataRate();
			//Send out bundles from DTN immediately (since COM has already told DTN its okay to send)
			sendDirect(msg, linkDelay+txDuration, 0, destinationModule, "gateToAir");
		}
	}
	else if (msg->getKind() == TIMESLOT_START_TIMER)
	{
		//Schedule the slot end message (only do this if we're actually using the tdma mac. when not using a mac slot endtime will be -1)
		if(mac->getSlotTimeEnd() != -1)
		{
			//TimeslotEndMsg *timeslotMsgEnd = new TimeslotEndMsg("timeslotEnd", TIMESLOT_END_TIMER);
			//timeslotMsgEnd->setSchedulingPriority(TIMESLOT_END_TIMER);
			//scheduleAt(mac->getSlotTimeEnd(), timeslotMsgEnd);

			//Instead of scheduling an END event, instead we just immediately check for the next slot in the future and scheule its start time



			//we need to grab the next hop node ID (for now an int, in the future could be a vector)
			//Also need the end of the current slot

			//Store these in a message to send to DTN layer
			QueryFromComMsg* queryMsg = new QueryFromComMsg("queryMsg", COM_QUERY_TO_DTN);
			queryMsg->setSchedulingPriority(COM_QUERY_TO_DTN);
			queryMsg->setSlotEnd(mac->getSlotTimeEnd());
			queryMsg->setNextHop(mac->getNextHopNodeID());
			//send to DTN layer
			send(queryMsg, "gateToDtn$o");


			//Schedule the next/future slot start
			simtime_t nextSlotTime = mac->getNextSlotTime();
			if(nextSlotTime < SIMTIME_MAX)
			{
				//Schedule the next slot start message
				TimeslotMsg *timeslotMsgStart = new TimeslotMsg("timeslotStart", TIMESLOT_START_TIMER);
				timeslotMsgStart->setSchedulingPriority(TIMESLOT_START_TIMER);
				//schedule
				scheduleAt(nextSlotTime, timeslotMsgStart);
			}

		}
		else
		{
			//We are using no/basic mac, no timeslot (or in other words just a slot from 0 to simtime_max)
			//No need to schedule an timeslotend message, only inform the dtn at time zero to transmit
			QueryFromComMsg* queryMsg = new QueryFromComMsg("queryMsg", COM_QUERY_TO_DTN);
			queryMsg->setSchedulingPriority(COM_QUERY_TO_DTN);
			queryMsg->setSlotEnd(SIMTIME_MAX);
			queryMsg->setNextHop(mac->getNextHopNodeID());
			//send to DTN layer
			send(queryMsg, "gateToDtn$o");
		}




	}
	/*else if (msg->getKind() == TIMESLOT_END_TIMER)
	{
		simtime_t nextSlotTime = mac->getNextSlotTime();
		//The tdma mac sets next slot time to simtime_max when there are no remaining slots for that node, detect this here
		if(nextSlotTime < SIMTIME_MAX)
		{
			//Schedule the next slot start message
			TimeslotMsg *timeslotMsgStart = new TimeslotMsg("timeslotStart", TIMESLOT_START_TIMER);
			timeslotMsgStart->setSchedulingPriority(TIMESLOT_START_TIMER);
			//schedule
			scheduleAt(nextSlotTime, timeslotMsgStart);
		}


	}*/
}


void Com::finish()
{
}

Com::Com()
{

}

Com::~Com()
{

}

