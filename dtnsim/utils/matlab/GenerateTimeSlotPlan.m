function [ ] = GenerateTimeSlotPlan(G,t_vec,contactplanfilename,numNodes)
%REQUIRES bfs (Breadth first search) function from the GAIMC library 
%Available https://www.mathworks.com/matlabcentral/fileexchange/24134-gaimc---graph-algorithms-in-matlab-code

%Function takes in either a time varying adjacency matrix or the contact
%plan file (in the format used by the dtnsim code)

%Usage (for a 3d adjacenct matrix):
%GenerateTimeSlotPlan(G,t_vec,[],numNodes)

%Usage (for a contact plan):
%GenerateTimeSlotPlan([],[],'contactplan_file.txt',numNodes)

%Function generates an adjacency matrix (if its not supplied) and then
%loops through it identifying each group where nodes have contacts with
%eachother. For example, if we have a network like A-B-C and D-E we would
%have two groups one of size 3 and one of size 2. A group ends when one of
%the nodes moves out of the group or another node moves into the group.

%The group list is looped over and timeslots are assigned proportionally.
%For example, if a group has 3 members and exists for 9 seconds, each node
%would get a 3 second timeslot. The ordering of the slots is determined
%randomly. 

%Output: A timeslot plan file to be used by dtnsim that has a line for every member in every
%group listing the group start end, the nodes first slot time, the nodes
%slot width, the other slots cumulative widths, and the group order/node
%number.

%TODO: More advanced grouping functionality - minimum timeslot width,
%repeating timeslots in a group, identifying special groups (e.g. say we
%have two groupings of nodes with only 1 link between them - the current
%code considers the entire thing one big group as they are connected and
%assigns them all equal time,however it would likely be better to assign 
%more slot time to the bottleneck link)
if isempty(G)
    disp('Parsing contactplan file');
    %using timeslot file, read in and generate connectivity matrix
    [a,contact,time_start,time_end,source_node,dest_node,datarate]= textread(contactplanfilename,'%s %s %f %f %f %f %f');
     %Grab only the contacts, ignore the ranges
     y = find(ismember(contact,'range'));
     t_start = time_start(1:y(1)-1);
     t_end = time_end(1:y(1)-1);
     src = source_node(1:y(1)-1);
     dst = dest_node(1:y(1)-1);
     rate = datarate(1:y(1)-1);
     min_time =min(t_start);
     max_time = max(t_end);
     t = min_time:1:max_time;
     G=zeros(numNodes,numNodes,length(t));
     %loop through and create connectivity matrix
     
     access_times = {};
     for j = 1:numNodes
         for k =1:numNodes
             %place each contact into a cell
             tmp = find(src == j & dst == k);
             if ~isempty(tmp)
                 %found indices with contacts that satisfy
                 access_times{j,k} = [t_start(tmp) , t_end(tmp)];
                 access_times{k,j} = [t_start(tmp), t_end(tmp)];
             end
         end
     end
     
     for i=1:length(t)
         %nodes start at 1 not 0
         for j=1:numNodes
             for k = 1:numNodes
                 if ~isempty(access_times{j,k})
                     %found access, see if it satisifes current time
                     tmp = access_times{j,k};
                     for m = 1:length(tmp(:,1))
                         contact_start = tmp(m,1);
                         contact_end = tmp(m,2);
                         if t(i) <= contact_end && t(i) >= contact_start
                             %found valid contact store it
                             G(j,k,i) = 1;
                             G(k,j,i) = 1;
                         end
                     end
                 end
             end
         end
     end
                 
     
elseif isempty(contactplan)
    %using already supplied connectivity matrix
    disp('Using pre-supplied connectivity matrix');
    %make sure diags are zero to be compatible with old code
    for i = 1:length(t_vec)
        G(:,:,i) = G(:,:,i) - diag(diag(G(:,:,i)));
    end
    t = t_vec;
end

%Now compute the timeslot groups per supplied options and save to file
numSlice = length(t);
group_list_stable = cell(100000,1);
group_list_unstable = cell(100,1);    
satIsGrouped = zeros(1,numNodes);
index_vec = 1:1:numNodes;
counter=1;

for i = 1:numSlice
    for j = 1:length(group_list_unstable(:,1))
        if ~isempty(group_list_unstable{j,1})
            %loop thru all group members, and add up their neighbors via
            %the connectivity graph
            old_members = group_list_unstable{j,1}{1,3};
            if length(old_members) == 1
                %this was a single member group, only need to check that
                %we still have zero neighbors or not
                my_neighbors = squeeze(G(old_members(1),:,i));
                if any(my_neighbors)
                    %changed, set current members to 2 to trigger group
                    %save
                    current_members = [1,2];
                else
                    current_members = old_members;
                end
            else
                current_members = find(bfs(squeeze(G(:,:,i)),old_members(1)) ~= -1);
            end
            %tmp_nodes = squeeze(G(old_members,:,i));
            %current_members = sort(unique(nonzeros(index_vec.*tmp_nodes)));
            
            if length(current_members) ~= length(old_members) || any(current_members ~= old_members)
                %membership has changed, save the group to stable, and
                %specify all of the group's members as ungrouped
                %if old_members === len1 dont save group
                if length(old_members) > 1
                    group_list_unstable{j,1}{1,2} = t(i-1);
                    group_list_stable{counter,1} = group_list_unstable{j,1};
                    counter=counter+1;
                end
                group_list_unstable{j,1} = [];
                satIsGrouped(old_members) = 0;
            end
        end
    end
    %Now we loop thru the ungrouped satellite list
    for j = 1:length(satIsGrouped)
        if satIsGrouped(j) == 0
            %Ungrouped satellite, find all its neighbors and create new
            %group
            my_neighbors = squeeze(G(j,:,i));
            %j is in this since the connectivity matrix does not have
            %G(i,i)=1 so we need to include self in group membership
            %new_members = sort(unique([nonzeros(index_vec.*my_neighbors); j]));
            if any(my_neighbors)
                new_members = find(bfs(squeeze(G(:,:,i)),j) ~= -1);
            else
                %this is a single membership group
                new_members = j;
            end
            group_idx = find_first_empty_slot(group_list_unstable);
            group_list_unstable{group_idx,1} = {t(i),-1,new_members};
            %finally, specify all of these members as grouped
            satIsGrouped(new_members) = 1;
        end
    end
end


%Now compute some statistics based on the groups
uptime = [];
group_size=[];
for i=1:length(group_list_stable(:,1))
    if ~isempty(group_list_stable{i,1})
        uptime = [uptime (group_list_stable{i,1}{1,2} - group_list_stable{i,1}{1,1})];
        group_size = [group_size length(group_list_stable{i,1}{1,3})];
    end
end

figure(1);
hist(uptime,100)
xlabel('uptime (seconds)')
figure(2);
hist(group_size,20)
xlabel('Group size')

disp('Mean group existences time:')
mean(uptime)
disp('Mean group size:')
mean(group_size)

fileID = fopen('timeslotplan.txt','w');
%Now we loop through the stable group list, split apart into timeslots aaccording to number of nodes in group and
%write to file. one line for each group
for i = 1:length(group_list_stable(:,1))
    if ~isempty(group_list_stable{i,1})
        %compute slots equally amongst group members
        members = group_list_stable{i,1}{1,3};
        t_s = group_list_stable{i,1}{1,1};
        t_e = group_list_stable{i,1}{1,2};
        total_width = t_e-t_s;
        %Note ordering of nodes within the contact is randomized!
        ordering = randperm(length(members));
        
        inds = find(t>= t_s & t<= t_e);
        for j = 1:length(members)
            my_width = (1/length(members))*total_width;
            remain = total_width - my_width;
            my_first = (ordering(j)-1)*my_width + t_s;
            %Hacky modification to also print the destination of each node - we
            %get this from the connectivity matrix - right now it is just a
            %single number but in the future it could be many nodes that we
            %could reach on one contact
            dest_node = find(G(members(j),:,inds(1)) == 1);
            fprintf(fileID, '%i %f %f %f %f %f %i %i %i \n', members(j),t_s,t_e,my_first,my_width,remain,length(members),ordering(j),dest_node);
        end
    end
end
end

