/*
 * RoutingDeterministic.cpp
 *
 *  Created on: Jul 18, 2017
 *      Author: FRaverta
 */

#include <dtn/routing/RoutingDeterministic.h>

RoutingDeterministic::RoutingDeterministic(int eid, SdrModel * sdr, ContactPlan * contactPlan)
	:Routing(eid,sdr)
{
	contactPlan_ = contactPlan;
}

RoutingDeterministic::~RoutingDeterministic()
{
}

void RoutingDeterministic::msgToOtherArrive(BundlePkt * bundle, double simTime)
{
	// Route and enqueue bundle
	routeAndQueueBundle(bundle, simTime);
}

bool RoutingDeterministic::msgToMeArrive(BundlePkt * bundle)
{
	return true;
}

void RoutingDeterministic::contactStart(Contact *c)
{

}

void RoutingDeterministic::successfulBundleForwarded(long bundleId, Contact * c,  bool sentToDestination)
{

}

void  RoutingDeterministic::refreshForwarding(Contact * c)
{

}

std::vector<long> RoutingDeterministic::contactEnd(Contact *c)
{
	std::vector<long> bundle_id_list;
	while (sdr_->isBundleForContact(c->getId()))
	{
		BundlePkt* bundle = sdr_->getNextBundleForContact(c->getId());
		bundle_id_list.push_back(bundle->getBundleId());
		sdr_->popNextBundleForContact(c->getId());

		//emit(dtnBundleReRouted, true);
		routeAndQueueBundle(bundle, simTime().dbl());
	}
	return bundle_id_list;
}




