/*
 * mac.h
 *
 *  Created on: January 28, 2018
 *      Author: MCarosino
 *
 * This file declares an interface that any mac method must implement.
 *
 */

#ifndef SRC_NODE_COM_MAC_H_
#define SRC_NODE_COM_MAC_H_

#include <dtn/ContactPlan.h>
#include <cstdio>
#include <string>
#include <omnetpp.h>
#include <fstream>
#include <sstream>
#include <map>
#include <queue>
#include <vector>
#include <tuple>
#include "dtnsim_m.h"

using namespace omnetpp;
using namespace std;


class MAC
{
public:
	MAC()
	{

	}
	virtual ~MAC()
	{
	}
	//making these standard virtual so I can give a definition here for the base MAC, still need to implement them in the derived class

	//Return time of a node's very first timeslot (return 0 when not using a MAC)
	virtual simtime_t getFirstSlotTime() {return SIMTIME_ZERO;}

	//Return end of the current slot - should ONLY be called while we are inside a valid timeslot! (return -1 when not using a MAC)
	virtual simtime_t getSlotTimeEnd() {return -1;}

	//Return the next valid time to transmit. There are only 2 options, simTime() (Now) or the next slot start time (future)
	//(return current time when not using a MAC)
	virtual simtime_t getNextTransmitTime() {return simTime();}

	//Return the start of the next slot, regardless of if we are currently in a valid slot or not. (return current time when not using a MAC)
	virtual simtime_t getNextSlotTime() {return simTime();}

	//Returns the nexthop node id for the current contact. (return -1 indicating unknown when not using a MAC)
	virtual int getNextHopNodeID() {return -1;}

protected:

};

#endif /* SRC_NODE_COM_MAC_H_ */
