# DTNSIM #

DTNSIM is a simulator devoted to study different aspects of Delay/Disruption Tolerant Network (DTN) such as routing, forwarding, scheduling, planning, and others.
The simulator is written and maintained by Juan Fraire and Pablo Madoery ( madoerypablo@gmail.com and juanfraire@gmail.com )
For installation information, please see the original repo readme https://bitbucket.org/lcd-unc-ar/dtnsim/overview

## Integration of MAC layer support ##
This code builds off the original dtnsim to add MAC layer support to the simulation. In the original dtnsim code, no MAC layer is assumed which allows multiple nodes to communicate without any interference.
While this is technically achievable in practice (e.g. satellites with multiple antennas, using multiple frequencies, cdma strategies), there are many scenarios and missions that satellites may only have a limited number of antennas (possibly isotropic) and limited frequency band usage - necessitating a medium access control method to prevent collisions and interference.

As a first step towards achieving MAC layer support, we have written a basic TDMA capability as a "MAC" object under the "Com" module of dtnsim. A pre-computed timeslot plan is distributed to all of the node's com layers and used to determine when it is okay to send bundles. Below we summarize the detailed functionality of the various code additions.


# Todo Items and Future Work #
* More advanced MAC protocols like CSMA

* Timeslot creation via locally connected groups is very basic. For exmaple, scenarios where many satellites might organize into a long line type network would result in the script identifying this as a group and assigning them all a small timeslot despite it being likely that each satellite can only hear transmissions from his immediate neighbors and not from satellites many hops away. This could be remedied both by re-thinking how we create satellite groups and also incorporating more complexity than just basic TDMA MAC by itself.

* The DTN layer is entirely unaware of the timeslotting. There are a number of solutions to this:
	* 1) The dtn layer is supplied with updated contact volumes that are reduced according to the fraction of time that a node is given to transmit during the contact.
	* 2) The dtn layer is supplied with a modified contact plan that is broken up according to the timeslot plan. E.g. if we have a contact between two nodes i,j for time t=0 to t=5 and each would get a 	          timeslot of 2.5 seconds, then we would split the contact into one for i->j time t=0->2.5 and one for j->i time t=2.5->5.0
	* 3) A merge and/or cross layer optimization of the dtn + com layers. The advantage of this approach is the ease of generalization to other mac schemes beyond tdma.
	
* The COM/MAC treats all nodes the same currently however it would be more realistic to consider ground stations as having significantly more resources and differences from satellites (such as having more antennas, communicating on multiple or different frequency bands, etc.)

* Investigate more complex TDMA schemes such as Decentralized TDMA where nodes could negotiate timeslots as needed from nodes that dont have bundles to transmit

* If a bundle is sent to the Com layer that is larger than can fit in one of the timeslots, it will sit there blocking any transmissions until a larger timeslot occurs or the contact finishes and the bundle is removed. Two possible solutions: 1) The com layer searches the entire queue for any bundles that fit in the current slot and transmits the oldest one. 2) Merge of the dtn / Com layer prevents the situation from happening.


# Usage #
Currently an example .ini usecase is located in useCases/tdma_testing/hindawi_rrn_tdma.ini which is based off the original author's walker constellation simulation.

## Timeslot Plan Creation ##

The GenerateTimeSlotPlan.m in the utils/matlab/ folder takes in the contact plan (in normal dtnsim format) and computes a timeslot plan. The network is split up into locally connected groups of nodes (in other words, if I'm at node i, any node j that I currently have a path to will be in my group. Once one or more of these nodes leaves the group or a new node joins the group, we conclude that group and start a new one if necessary).

If a group exists for k seconds and there are N nodes in that group, each node will get a k/N second timeslot. The ordering of timeslots is chosen randomly. Nodes that have no neighbors (i.e. no contacts) are not put into a group. The script writes a text file with a line for every node in every group (a node may belong to many different groups over the course of the simulation but never overlapping) as follows:

i  t_s  t_e  t_w  t_r  N  j

where i is the node EID in the group, t_s is the time the group began, t_e is the time the group ended, t_w is the width of node i's first time slot, t_r is the sum of the other group member's timeslots widths, N is the number of nodes in the group, j is the ordering number of node i's timeslot (the last 2 fields currently unused in the sim).

The created timeslotplan.txt file should be placed useCases/tdma_testing/timeSlotPlan/ folder. An example one is already there that was created using the original dtnsim's hindawi_rrn Walker contact plan.

## Timeslot Plan Parsing ##

In the central.cc init function, the timeslot plan is read in via the TimeSlotPlan class (modeled after the ContactPlan class) and stored as a vector of tuples. The plan for each specific node, k, is extracted ( all lines in the above notation where i = k for example) and passed to the Com module.

## MAC Initialization ##

In the Com.cc init function, the MAC object is instantiated based upon the user specificed parameters in the .ini file (current choices None or TDMA Centralized). The MAC object is given the timeslot plan in its constructor.

## MAC Functionality ##

The purpose of the MAC object is to handle the state keeping required to determine when the Com module can transmit. The main function to handle this is the updateNextStateAndSlot which can be used to either verify that the current state is correct or to jump to the next state.

There are four functions which return various data to the Com module like getting slot start and end times and determining when the next possible transmit time is. 

## Com Modifications ##

The Com module was modified so that incoming bundles wanting to be forwarded would instead be added to a queue. Additionaly, the beginning of a timeslot is scheduled via a TimeslotMsg - this allows incoming bundles to wake up the sending thread similar to how the dtn.cc code works.

Upon reception of the TimeslotMsg, if there are bundles waiting in the queue, it verifies that both A) the bundle can still be sent before the contact ends (info from the dtn module that's stored in the bundle's ttlCom field) and B) that the bundle can be sent before the timeslot ends (by querying the MAC's getSlotTimeEnd() function). If both of these are true the bundle is sent to the destination com module and removed from the queue.

If after sending there are bundles still in the queue, a new timeslotmsg is scheduled either txduration seconds in the future (if the current slot will still be active at this time) or at the next possible timeslot.

Currently the DTN module is mainyl unmodified, only changes include storing the contact data rate and contact end time into a bundles meta data for the com module to use when computing transmission times.


## Contact Me ##

E-mail at mcaros at uw.edu Thanks!