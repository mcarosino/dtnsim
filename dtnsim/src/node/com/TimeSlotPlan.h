/*
 * TimeSlotPlan.h
 *
 *  Created on: Feb 12, 2018
 *      Author: micha
 */

#ifndef SRC_NODE_COM_TIMESLOTPLAN_H_
#define SRC_NODE_COM_TIMESLOTPLAN_H_


#include <omnetpp.h>
#include <vector>
#include <fstream>

using namespace std;
using namespace omnetpp;

class TimeSlotPlan {

public:

	TimeSlotPlan();
	virtual ~TimeSlotPlan();

	// Contact plan population functions
	void parseTimeSlotPlanFile(string fileName);

	// Contact plan exploration functions
	std::vector<std::tuple<simtime_t,simtime_t, simtime_t,simtime_t,simtime_t,int,int,int>> getPlanForNode(int eid);


private:
	std::vector<std::tuple<int,simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> timeSlotPlan_;
	void addGroup(int,double,double,double,double,double,int,int,int);

};



#endif /* SRC_NODE_COM_TIMESLOTPLAN_H_ */
