#ifndef SRC_NODE_COM_MAC_MACTDMACENTRALIZED_H_
#define SRC_NODE_COM_MAC_MACTDMACENTRALIZED_H_

#include <com/mac/mac.h>

//offsets of the various information stored for each timeslot grouping
#define GROUP_START 0
#define GROUP_END 1
#define FIRST_SLOT 2
#define SLOT_WIDTH 3
#define REMAIN_WIDTH 4
#define GROUP_SIZE 5
#define GROUP_PRIO 6
#define NEXT_HOP 7

class MACTDMACentralized: public MAC {
public:
	MACTDMACentralized(int eid, int nodeNum, double guardBandPercent, std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> &timeSlotPlan);
	virtual ~MACTDMACentralized();

	virtual simtime_t getFirstSlotTime();
	virtual simtime_t getNextSlotTime();
	virtual simtime_t getSlotTimeEnd();
	virtual simtime_t getNextTransmitTime();

	virtual int getNextHopNodeID();


private:
	int nodeNum_;
	int eid_;
	int currentIndex_;
	int currentSlotNumber_;
	double guardBandPercent_;

	simtime_t transmitSlotWidth_;
	simtime_t fullSlotWidth_;
	simtime_t currentSlotStart_;
	simtime_t currentSlotEnd_;
	simtime_t otherNodeSlotWidths_;

	int currentNextHopNodeID_;

	std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> timeSlotPlan_ ;

	std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int> currentState_;

	void updateNextStateAndSlot(int force);


};


#endif /* SRC_NODE_COM_MAC_MACTDMACENTRALIZED_H_ */
