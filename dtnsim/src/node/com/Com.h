#ifndef COM_H_
#define COM_H_

#include <com/mac/MACTDMACentralized.h>
#include <dtn/ContactPlan.h>
#include <com/TimeSlotPlan.h>

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include <fstream>
#include <iomanip>

#include "MsgTypes.h"
#include "dtnsim_m.h"

using namespace std;
using namespace omnetpp;

class Com: public cSimpleModule
{
public:
	Com();
	virtual ~Com();
	virtual void setContactTopology(ContactPlan &contactTopology);
	virtual void setTimeSlotPlan(std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> timeSlotPlan);
	//virtual void refreshTransmit();

protected:
	virtual int numInitStages() const;
	virtual void initialize(int stage);
	virtual void handleMessage(cMessage *);
	virtual void finish();

private:

	MAC * mac;

	int eid_;
	ContactPlan contactTopology_;
	std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> timeSlotPlan_;

	double packetLoss_;
	list<cMessage *> bundleQueue;
	std::map<uint32_t, TimeslotMsg *> timeslotMsgStart_;

	//Signals
	simsignal_t comBundlesQueued;
	simsignal_t comBundlesSent;

};

#endif /* COM_H_ */
