#include <dtn/Dtn.h>
#include "App.h"

Define_Module (Dtn);

Dtn::Dtn()
{

}

Dtn::~Dtn()
{

}

void Dtn::setContactPlan(ContactPlan &contactPlan)
{
	this->contactPlan_ = contactPlan;
}

void Dtn::setContactTopology(ContactPlan &contactTopology)
{
	this->contactTopology_ = contactTopology;
}

int Dtn::numInitStages() const
{
	int stages = 2;
	return stages;
}

void Dtn::initialize(int stage)
{
	if (stage == 1)
	{
		// Store this node eid
		this->eid_ = this->getParentModule()->getIndex();

		this->custodyTimeout_ = par("custodyTimeout");
		this->custodyModel_.setEid(eid_);
		this->custodyModel_.setSdr(&sdr_);
		this->custodyModel_.setCustodyReportByteSize(par("custodyReportByteSize"));

		// Get a pointer to graphics module
		graphicsModule = (Graphics *) this->getParentModule()->getSubmodule("graphics");
		// Register this object as sdr obsever, in order to display bundles stored in sdr properly.
		sdr_.addObserver(this);
		update();

		const char *dstNodesNoMAC = par("dstWithoutMAC");
		cStringTokenizer dstNodesNoMACTokenizer(dstNodesNoMAC, ",");
		while (dstNodesNoMACTokenizer.hasMoreTokens())
			dstNodesNoMACVec_.push_back(atoi(dstNodesNoMACTokenizer.nextToken()));

		// Schedule local starts contact messages.
		// Only contactTopology starts contacts are scheduled.
		vector<Contact> localContacts1 = contactTopology_.getContactsBySrc(this->eid_);
		for (vector<Contact>::iterator it = localContacts1.begin(); it != localContacts1.end(); ++it)
		{
			ContactMsg *contactMsgStart = new ContactMsg("contactStart", CONTACT_START_TIMER);

			contactMsgStart->setSchedulingPriority(CONTACT_START_TIMER);
			contactMsgStart->setId((*it).getId());
			contactMsgStart->setStart((*it).getStart());
			contactMsgStart->setEnd((*it).getEnd());
			contactMsgStart->setDuration((*it).getEnd() - (*it).getStart());
			contactMsgStart->setSourceEid((*it).getSourceEid());
			contactMsgStart->setDestinationEid((*it).getDestinationEid());
			contactMsgStart->setDataRate((*it).getDataRate());

			scheduleAt((*it).getStart(), contactMsgStart);

			EV << "node " << eid_ << ": " << "a contact +" << (*it).getStart() << " +" << (*it).getEnd() << " " << (*it).getSourceEid() << " " << (*it).getDestinationEid() << " " << (*it).getDataRate() << endl;
		}
		// Schedule local ends contact messages.
		// All ends contacts of the contactPlan are scheduled.
		// to trigger re-routings of bundles queued in contacts that did not happen.
		vector<Contact> localContacts2 = contactPlan_.getContactsBySrc(this->eid_);
		for (vector<Contact>::iterator it = localContacts2.begin(); it != localContacts2.end(); ++it)
		{
			ContactMsg *contactMsgEnd = new ContactMsg("contactEnd", CONTACT_END_TIMER);

			contactMsgEnd->setName("ContactEnd");
			contactMsgEnd->setSchedulingPriority(CONTACT_END_TIMER);
			contactMsgEnd->setId((*it).getId());
			contactMsgEnd->setStart((*it).getStart());
			contactMsgEnd->setEnd((*it).getEnd());
			contactMsgEnd->setDuration((*it).getEnd() - (*it).getStart());
			contactMsgEnd->setSourceEid((*it).getSourceEid());
			contactMsgEnd->setDestinationEid((*it).getDestinationEid());
			contactMsgEnd->setDataRate((*it).getDataRate());

			scheduleAt((*it).getStart() + (*it).getDuration(), contactMsgEnd);
		}

		// Initialize routing
		this->sdr_.setEid(eid_);
		this->sdr_.setSize(par("sdrSize"));
		this->sdr_.setNodesNumber(this->getParentModule()->getParentModule()->par("nodesNumber"));
		this->sdr_.setContactPlan(&contactPlan_);

		string routeString = par("routing");

		if (routeString.compare("direct") == 0)
			routing = new RoutingDirect(eid_, &sdr_, &contactPlan_);
		else if (routeString.compare("cgrModel350") == 0)
			routing = new RoutingCgrModel350(eid_, &sdr_, &contactPlan_, par("printRoutingDebug"));
		else if (routeString.compare("cgrModel350_3") == 0)
			routing = new RoutingCgrModel350_3(eid_, &sdr_, &contactPlan_, par("printRoutingDebug"));
		else if (routeString.compare("cgrModelYen") == 0)
			routing = new RoutingCgrModelYen(eid_, &sdr_, &contactPlan_, par("printRoutingDebug"));
		else if (routeString.compare("cgrModelRev17") == 0)
		{
			ContactPlan * globalContactPlan = ((Dtn *) this->getParentModule()->getParentModule()->getSubmodule("node", 0)->getSubmodule("dtn"))->getContactPlanPointer();
			routing = new RoutingCgrModelRev17(eid_, this->getParentModule()->getVectorSize(), &sdr_, &contactPlan_, globalContactPlan, par("routingType"), par("printRoutingDebug"));
		}
		else if (routeString.compare("cgrIon350") == 0)
		{
			int nodesNumber = this->getParentModule()->getParentModule()->par("nodesNumber");
			routing = new RoutingCgrIon350(eid_, &sdr_, &contactPlan_, nodesNumber);
		}
		else if (routeString.compare("epidemic") == 0)
		{
			routing = new RoutingEpidemic(eid_, &sdr_, this);
		}
		else if (routeString.compare("sprayAndWait") == 0)
		{
			int bundlesCopies = par("bundlesCopies");
			routing = new RoutingSprayAndWait(eid_, &sdr_, this, bundlesCopies, false);
		}
		else if (routeString.compare("binarySprayAndWait") == 0)
		{
			int bundlesCopies = par("bundlesCopies");
			routing = new RoutingSprayAndWait(eid_, &sdr_, this, bundlesCopies, true);
		}
		else if (routeString.compare("cgrModel350_Proactive") == 0)
			routing = new RoutingCgrModel350_Proactive(eid_, &sdr_, &contactPlan_, par("printRoutingDebug"), this);
		else if (routeString.compare("cgrModel350_Probabilistic") == 0)
		{
			double sContactProb = par("sContactProb");
			routing = new RoutingCgrModel350_Probabilistic(eid_, &sdr_, &contactPlan_, par("printRoutingDebug"), this, sContactProb);
		}
		else
		{
			cout << "dtnsim error: unknown routing type: " << routeString << endl;
			exit(1);
		}

		// Register signals
		dtnBundleSentToCom = registerSignal("dtnBundleSentToCom");
		dtnBundleSentToApp = registerSignal("dtnBundleSentToApp");
		dtnBundleSentToAppHopCount = registerSignal("dtnBundleSentToAppHopCount");
		dtnBundleSentToAppRevisitedHops = registerSignal("dtnBundleSentToAppRevisitedHops");
		dtnBundleReceivedFromCom = registerSignal("dtnBundleReceivedFromCom");
		dtnBundleReceivedFromApp = registerSignal("dtnBundleReceivedFromApp");
		dtnBundleReRouted = registerSignal("dtnBundleReRouted");
		sdrBundleStored = registerSignal("sdrBundleStored");
		sdrBytesStored = registerSignal("sdrBytesStored");
		routeCgrDijkstraCalls = registerSignal("routeCgrDijkstraCalls");
		routeCgrDijkstraLoops = registerSignal("routeCgrDijkstraLoops");
		routeCgrRouteTableEntriesCreated = registerSignal("routeCgrRouteTableEntriesCreated");
		routeCgrRouteTableEntriesExplored = registerSignal("routeCgrRouteTableEntriesExplored");

		if (eid_ != 0)
		{
			emit(sdrBundleStored, sdr_.getBundlesCountInSdr());
			emit(sdrBytesStored, sdr_.getBytesStoredInSdr());
		}

		// Initialize BundleMap
		this->saveBundleMap_ = par("saveBundleMap");
		this->saveBundleMap2_ = par("saveBundleMap2");
		this->saveTimeslotMap_ = par("saveTimeslotMap");
		if (saveBundleMap_ && eid_ != 0)
		{
			// create result folder if it doesn't exist
			struct stat st =
			{ 0 };
			if (stat("results", &st) == -1)
			{
				mkdir("results", 0700);
			}

			string fileStr = "results/BundleMap_Node" + to_string(eid_) + ".csv";
			bundleMap_.open(fileStr);
			bundleMap_ << "SimTime" << "," << "SRC" << "," << "DST" << "," << "TSRC" << "," << "TDST" << "," << "BitLenght" << "," << "DurationSec" << endl;
		}
		if(saveBundleMap2_ && eid_ != 0)
		{
			// create result folder if it doesn't exist
			struct stat st =
			{ 0 };
			if (stat("results", &st) == -1)
			{
				mkdir("results", 0700);
			}

			string fileStr = "results/BundleMap2_Node" + to_string(eid_) + ".csv";
			bundleMap2_.open(fileStr);
			bundleMap2_ << "BundleID" << "," << "ContactID" << "," << "Status" << "," << "CurrentNode" << "," << "NextHopNode" << "," << "SRC" << "," << "DST" << "," << "BundleSize(B)" << "," << "ArrivalTime" << "," << "Contact Queue Length" << "," << "Contact Queue Size(B)" << endl;
		}
		if(saveTimeslotMap_ && eid_ != 0)
		{
			// create result folder if it doesn't exist
			struct stat st =
			{ 0 };
			if (stat("results", &st) == -1)
			{
				mkdir("results", 0700);
			}

			string fileStr = "results/TimeslotMap_Node" + to_string(eid_) + ".csv";
			timeslotMap_.open(fileStr);
			timeslotMap_ << "SlotStart" << "," << "SlotEnd" << "," << "NextHopID" << "," << "ContactID" << endl;
		}
	}
}

void Dtn::finish()
{
	// Last call to sample-hold type metrics
	if (eid_ != 0)
	{
		emit(sdrBundleStored, sdr_.getBundlesCountInSdr());
		emit(sdrBytesStored, sdr_.getBytesStoredInSdr());
	}

	// Delete scheduled forwardingMsg
//	std::map<int, ForwardingMsgStart *>::iterator it;
//	for (it = forwardingMsgs_.begin(); it != forwardingMsgs_.end(); ++it)
//	{
//		ForwardingMsgStart * forwardingMsg = it->second;
//		cancelAndDelete(forwardingMsg);
//	}
	for(std::map<int,std::tuple<int,int,simtime_t,simtime_t>>::iterator it = activeContactData_.begin(); it != activeContactData_.end(); it++ )
	{
		int cid = it->first;
		if(stateMsgs_.count(cid) != 0)
			cancelAndDelete(stateMsgs_[cid]);
		activeContactData_.erase(cid);

	}

	// Delete all stored bundles
	sdr_.freeSdr(eid_);

	// BundleMap End
	if (saveBundleMap_)
		bundleMap_.close();

	if (saveBundleMap2_)
		bundleMap2_.close();


	delete routing;
}

void Dtn::handleMessage(cMessage * msg)
{
	///////////////////////////////////////////
	// New Bundle (from App or Com):
	///////////////////////////////////////////
	if (msg->getKind() == BUNDLE || msg->getKind() == BUNDLE_CUSTODY_REPORT)
	{
		BundlePkt* bundle = check_and_cast<BundlePkt *>(msg);
		if (msg->arrivedOn("gateToCom$i"))
		{
			emit(dtnBundleReceivedFromCom, true);
		}
		if (msg->arrivedOn("gateToApp$i"))
		{
			emit(dtnBundleReceivedFromApp, true);
		}


		dispatchBundle(bundle);
	}

	///////////////////////////////////////////
	// Contact Start and End
	///////////////////////////////////////////
	else if (msg->getKind() == CONTACT_START_TIMER)
	{
		// Schedule end of contact
		ContactMsg* contactMsg = check_and_cast<ContactMsg *>(msg);

		// Visualize contact line on
		graphicsModule->setContactOn(contactMsg);

		// Call to routing algorithm (NOTE: For most routing algs this doesn't do anything)
		routing->contactStart(contactPlan_.getContactById(contactMsg->getId()));



		//If waitforcom is false, we should go ahead and schedule a state machine run through immediately
		if(waitForCom == false || std::binary_search(dstNodesNoMACVec_.begin(),dstNodesNoMACVec_.end(),contactMsg->getDestinationEid()))
		{
			//Add the contact to the list
			activeContactData_[contactMsg->getId()] = std::make_tuple(contactMsg->getDestinationEid(),READY_FOR_BUNDLE,contactMsg->getStart(),contactMsg->getEnd());
			StateMsg *stateMsg = new StateMsg("stateMsg", STATE_MACHINE);
			stateMsg->setSchedulingPriority(STATE_MACHINE);
			stateMsg->setContactId(contactMsg->getId());
			stateMsg->setBundleSentReport(false);
			//schedule
			scheduleAt(simTime(), stateMsg);
			//add to list
			stateMsgs_[contactMsg->getId()] = stateMsg;
		}
		else
		{
			//Just add the contact to the list, state machine run will be triggered by incoming bundle or COM query only
			activeContactData_[contactMsg->getId()] = std::make_tuple(contactMsg->getDestinationEid(),TRANSMIT_DISABLED,SIMTIME_ZERO,SIMTIME_ZERO);
		}

		delete contactMsg;
	}
	else if (msg->getKind() == CONTACT_END_TIMER)
	{
		// Finish transmission: If bundles are left in contact re-route them
		ContactMsg* contactMsg = check_and_cast<ContactMsg *>(msg);

		for (int i = 0; i < sdr_.getBundlesCountInContact(contactMsg->getId()); i++)
			emit(dtnBundleReRouted, true);


		std::vector<long> bundle_id_list = routing->contactEnd(contactPlan_.getContactById(contactMsg->getId()));
		if(saveBundleMap2_)
		{
			//Need to iterate over the bunlde_id_list
			for (vector<long>::iterator it = bundle_id_list.begin(); it!=bundle_id_list.end(); ++it)
			{
				//Store bundle map information
				//Step 1, find the queue that the bundle exists in via it's ID
				int tmp_cid = sdr_.getContactIdForBundleId(*it);
				int queue_len = -1;
				int queue_size = -1;
				int tmp_dst = -1;
				string status;
				if(tmp_cid != -1)
				{
					//Grab the length of the queue for this CID
					queue_len = sdr_.getBundlesCountInContact(tmp_cid);
					if(tmp_cid != 0)
					{
						queue_size = sdr_.getBytesStoredToContact(tmp_cid);
						//finally grab the contact to figure out the dest
						Contact * contact = contactTopology_.getContactById(tmp_cid);
						tmp_dst =contact->getDestinationEid();
						status = "queued-rerouted";
					}
					else
					{
						status = "limbo";
					}
				}
				else
				{
					status = "deleted";
				}
				bundleMap2_ << *it << "," << tmp_cid << "," << status << "," << this->eid_ << "," << tmp_dst << "," << -1 << "," << -1 << "," << -1 << "," << simTime().dbl() << "," << queue_len << "," << queue_size << endl;
			}

		}
		// Visualize contact line off
		graphicsModule->setContactOff(contactMsg);

		// Delete contactMsg
		//cancelAndDelete(forwardingMsgs_[contactMsg->getId()]);
		//forwardingMsgs_.erase(contactMsg->getId());
		//delete contactMsg;

		//remove contact from list
		//TODO DO WE NEED O KEEP TRACK OF STATE MACHINE MSG SO WE CAN CANCEL THEM?
		//cancelAndDelete(activeContactData_[contactMsg->getId()]);
		if(stateMsgs_.count(contactMsg->getId()) != 0)
		{
			cancelEvent(stateMsgs_[contactMsg->getId()]);
			//If this is a bundle transmission report, do it here
			if(stateMsgs_[contactMsg->getId()]->getBundleSentReport() == true)
			{
				int bundleId = stateMsgs_[contactMsg->getId()]->getBundleId();
				Contact * contact = contactTopology_.getContactById(contactMsg->getId());

				routing->successfulBundleForwarded(bundleId, contact, stateMsgs_[contactMsg->getId()]->getSentToDestination());
			}
			//Finally delete
			stateMsgs_.erase(contactMsg->getId());
		}

		activeContactData_.erase(contactMsg->getId());
		delete contactMsg;

		//Finally refresh contacts to ensure that any re-routed bundles for active contacts are IMMEDIATELY sent
		if(bundle_id_list.size() > 0)
			this->refreshForwarding();
	}
	else if (msg->getKind() == STATE_MACHINE)
	{
		StateMsg* stateMsg = check_and_cast<StateMsg *>(msg);
		int contactId = stateMsg->getContactId();
		//Erase the copy of the scheduled message
		stateMsgs_.erase(contactId);

		//Check if state msg is occurring due to a finished bundle
		if(stateMsg->getBundleSentReport() == true)
		{
			//Bundle has finished, perform ending steps
			int bundleId = stateMsg->getBundleId();
			Contact * contact = contactTopology_.getContactById(contactId);

			routing->successfulBundleForwarded(bundleId, contact, stateMsg->getSentToDestination());
			//Finally, change STATE to ready for bundle
			get<STATE_INDEX>(activeContactData_[contactId]) = READY_FOR_BUNDLE;
		}

		int state = get<STATE_INDEX>(activeContactData_[contactId]);
		int neighborEid = get<NEIGHBOR_EID_INDEX>(activeContactData_[contactId]);

		if(state == READY_FOR_BUNDLE)
		{
			if(simTime() < get<TRANSMIT_END_INDEX>(activeContactData_[contactId]))
			{
				//See if we have a bundle
				if (sdr_.isBundleForContact(contactId))
				{
					// If local/remote node are responsive, then transmit bundle
					Dtn * neighborDtn = check_and_cast<Dtn *>(this->getParentModule()->getParentModule()->getSubmodule("node", neighborEid)->getSubmodule("dtn"));
					if ((!neighborDtn->onFault) && (!this->onFault))
					{
						// Get bundle pointer from sdr
						BundlePkt* bundle = sdr_.getNextBundleForContact(contactId);

						// Calculate data rate and Tx duration
						double dataRate = contactTopology_.getContactById(contactId)->getDataRate();

						//Store datarate in bundle metadata for COM layer usage!!
						bundle->setDataRate(dataRate);

						double txDuration = (double) bundle->getByteLength() / dataRate;
						double linkDelay = contactTopology_.getRangeBySrcDst(eid_, neighborEid);

						Contact * contact = contactTopology_.getContactById(contactId);

						// if the message can be fully transmitted before the end of the contact AND can be transmitted before end of the timeslot, transmit it
						if ((simTime() + txDuration + linkDelay) <= contact->getEnd() && simTime() + txDuration <= get<TRANSMIT_END_INDEX>(activeContactData_[contactId]))
						{
							// Set bundle metadata (set by intermediate nodes)
							bundle->setSenderEid(eid_);
							bundle->setHopCount(bundle->getHopCount() + 1);
							bundle->getVisitedNodes().push_back(eid_);
							bundle->setXmitCopiesCount(0);

							//cout<<"-----> sending bundle to node "<<bundle->getNextHopEid()<<endl;
							send(bundle, "gateToCom$o");

							if (saveBundleMap_)
								bundleMap_ << simTime() << "," << eid_ << "," << neighborEid << "," << bundle->getSourceEid() << "," << bundle->getDestinationEid() << "," << bundle->getBitLength() << "," << txDuration << endl;

							sdr_.popNextBundleForContact(contactId);

							if(saveBundleMap2_)
							{
								bundleMap2_ << bundle->getBundleId() << "," << contactId << "," << "SENT" << "," << this->eid_ << "," << contact->getDestinationEid() << "," << bundle->getSourceEid() << "," << bundle->getDestinationEid() << "," << bundle->getByteLength() << "," << simTime().dbl() << "," << sdr_.getBundlesCountInContact(contactId) << "," << sdr_.getBytesStoredToContact(contactId) << endl;

							}



							// If custody requested, store a copy of the bundle until report received
							if (bundle->getCustodyTransferRequested())
							{
								sdr_.enqueueTransmittedBundleInCustody(bundle->dup());
								this->custodyModel_.printBundlesInCustody();

								// Enqueue a retransmission event in case custody acceptance not received
								CustodyTimout * custodyTimeout = new CustodyTimout("custodyTimeout", CUSTODY_TIMEOUT);
								custodyTimeout->setBundleId(bundle->getBundleId());
								scheduleAt(simTime() + this->custodyTimeout_, custodyTimeout);
							}

							emit(dtnBundleSentToCom, true);
							emit(sdrBundleStored, sdr_.getBundlesCountInSdr());
							emit(sdrBytesStored, sdr_.getBytesStoredInSdr());

							//Now, schedule a new run of the state machine to a) notify bundle has finished and b) check for new bundles
							//Note that if this happens to occur exactly at the end of the contact, the CONTACTEND msg will take care of the bundle finish steps
							StateMsg *stateMsg_ = new StateMsg("stateMsg", STATE_MACHINE);
							stateMsg_->setSchedulingPriority(STATE_MACHINE);
							stateMsg_->setContactId(contactId);
							stateMsg_->setBundleId(bundle->getBundleId());
							stateMsg_->setSentToDestination(neighborEid == bundle->getDestinationEid());
							stateMsg_->setBundleSentReport(true);
							//schedule
							scheduleAt(simTime()+txDuration, stateMsg_);
							stateMsgs_[contactId] = stateMsg_;

							//Also place the state into bundle being transmitted
							get<STATE_INDEX>(activeContactData_[contactId]) = BUNDLE_IN_PROG;


						}
					}
				}
			}
			else
			{
				//curent time is past the last known transmit time, disable bundle transmission
				get<STATE_INDEX>(activeContactData_[contactId]) = TRANSMIT_DISABLED;
			}
		}
		delete stateMsg;
	}
	///////////////////////////////////////////
	// Custody retransmission timer
	///////////////////////////////////////////
	else if (msg->getKind() == CUSTODY_TIMEOUT)
	{
		// Custody timer expired, check if bundle still in custody memory space and retransmit it if positive
		CustodyTimout* custodyTimout = check_and_cast<CustodyTimout *>(msg);
		BundlePkt * reSendBundle = this->custodyModel_.custodyTimerExpired(custodyTimout);

		if(reSendBundle!=NULL)
			this->dispatchBundle(reSendBundle);

		delete custodyTimout;
	}
	else if (msg->getKind() == COM_QUERY_TO_DTN)
	{
		QueryFromComMsg * comMsg = check_and_cast<QueryFromComMsg *>(msg);
		//Update the current slot end and nexthop node ID state
		int nextHopCom = comMsg->getNextHop();
		for(std::map<int,std::tuple<int,int,simtime_t,simtime_t>>::iterator it = activeContactData_.begin(); it != activeContactData_.end(); it++ )
		{
			if(get<NEIGHBOR_EID_INDEX>(it->second) == nextHopCom)
			{
				if(saveTimeslotMap_)
					timeslotMap_ << simTime() << "," << comMsg->getSlotEnd() << "," << nextHopCom << "," << it->first << endl;

				get<TRANSMIT_START_INDEX>(it->second) = simTime();
				get<TRANSMIT_END_INDEX>(it->second) = comMsg->getSlotEnd();
				get<STATE_INDEX>(it->second) = READY_FOR_BUNDLE;
				//SCHEDULE A RUN THROUGH THE STATEMACHINE
				StateMsg *stateMsg = new StateMsg("stateMsg", STATE_MACHINE);
				stateMsg->setSchedulingPriority(STATE_MACHINE);
				stateMsg->setContactId(it->first);
				stateMsg->setBundleSentReport(false);
				//schedule
				scheduleAt(simTime(), stateMsg);
				//add to list
				stateMsgs_[it->first] = stateMsg;

			}
			else
			{
				//No contact exists for this next hop dest yet... in the future, create a contact for it!
			}

		}
		delete comMsg;
	}
}


void Dtn::dispatchBundle(BundlePkt *bundle)
{
	//char bundleName[10];
	//sprintf(bundleName, "Src:%d,Dst:%d(id:%d)", bundle->getSourceEid() , bundle->getDestinationEid(), (int) bundle->getId());
	//cout << "Dispatching " << bundleName << endl;

	if (this->eid_ == bundle->getDestinationEid())
	{
		// We are the final destination of this bundle
		emit(dtnBundleSentToApp, true);
		emit(dtnBundleSentToAppHopCount, bundle->getHopCount());
		bundle->getVisitedNodes().sort();
		bundle->getVisitedNodes().unique();
		emit(dtnBundleSentToAppRevisitedHops, bundle->getHopCount() - bundle->getVisitedNodes().size());

		// Check if this bundle has previously arrived here
		if (routing->msgToMeArrive(bundle))
		{
			// This is the first time this bundle arrives
			if (bundle->getBundleIsCustodyReport())
			{
				// This is a custody report destined to me
				BundlePkt * reSendBundle = this->custodyModel_.custodyReportArrived(bundle);

				// If custody was rejected, reroute
				if(reSendBundle!=NULL)
					this->dispatchBundle(reSendBundle);
			}
			else
			{
				// This is a data bundle destined to me
				if (bundle->getCustodyTransferRequested())
					this->dispatchBundle(this->custodyModel_.bundleWithCustodyRequestedArrived(bundle));

				// Send to app layer
				send(bundle, "gateToApp$o");
			}
		}
		else
			// A copy of this bundle was previously received
			delete bundle;
	}
	else
	{
		// This is a bundle in transit

		// Manage custody transfer
		if (bundle->getCustodyTransferRequested())
			this->dispatchBundle(this->custodyModel_.bundleWithCustodyRequestedArrived(bundle));

		// Either accepted or rejected custody, route bundle
		routing->msgToOtherArrive(bundle, simTime().dbl());

		// Emit routing specific statistics
		string routeString = par("routing");
		if (routeString.compare("cgrModel350") == 0)
		{
			emit(routeCgrDijkstraCalls, ((RoutingCgrModel350*) routing)->getDijkstraCalls());
			emit(routeCgrDijkstraLoops, ((RoutingCgrModel350*) routing)->getDijkstraLoops());
			emit(routeCgrRouteTableEntriesExplored, ((RoutingCgrModel350*) routing)->getRouteTableEntriesExplored());
		}
		if (routeString.compare("cgrModel350_3") == 0)
		{
			emit(routeCgrDijkstraCalls, ((RoutingCgrModel350_3*) routing)->getDijkstraCalls());
			emit(routeCgrDijkstraLoops, ((RoutingCgrModel350_3*) routing)->getDijkstraLoops());
			emit(routeCgrRouteTableEntriesExplored, ((RoutingCgrModel350_3*) routing)->getRouteTableEntriesExplored());
		}
		if (routeString.compare("cgrModelRev17") == 0)
		{
			emit(routeCgrDijkstraCalls, ((RoutingCgrModelRev17*) routing)->getDijkstraCalls());
			emit(routeCgrDijkstraLoops, ((RoutingCgrModelRev17*) routing)->getDijkstraLoops());
			emit(routeCgrRouteTableEntriesCreated, ((RoutingCgrModelRev17*) routing)->getRouteTableEntriesCreated());
			emit(routeCgrRouteTableEntriesExplored, ((RoutingCgrModelRev17*) routing)->getRouteTableEntriesExplored());
		}
		emit(sdrBundleStored, sdr_.getBundlesCountInSdr());
		emit(sdrBytesStored, sdr_.getBytesStoredInSdr());

		if(saveBundleMap2_)
		{
			//Store bundle map information
			//Step 1, find the queue that the bundle exists in via it's ID
			int tmp_cid = sdr_.getContactIdForBundleId(bundle->getBundleId());
			int queue_len = -1;
			int queue_size = -1;
			int tmp_dst = -1;
			string status;
			if(tmp_cid != -1)
			{
				//Grab the length of the queue for this CID
				queue_len = sdr_.getBundlesCountInContact(tmp_cid);
				if(tmp_cid != 0)
				{
					queue_size = sdr_.getBytesStoredToContact(tmp_cid);
					//finally grab the contact to figure out the dest
					Contact * contact = contactTopology_.getContactById(tmp_cid);
					tmp_dst =contact->getDestinationEid();
					status = "queued";
				}
				else
				{
					status = "limbo";
				}
			}
			else
			{
				status = "deleted";
			}
			bundleMap2_ << bundle->getBundleId() << "," << tmp_cid << "," << status << "," << this->eid_ << "," << tmp_dst << "," << bundle->getSourceEid() << "," << bundle->getDestinationEid() << "," << bundle->getByteLength() << "," << simTime().dbl() << "," << queue_len << "," << queue_size << endl;


		}


		// Wake-up sleeping forwarding threads
		this->refreshForwarding();
	}
}



void Dtn::refreshForwarding()
{
    // Check all on-going contacts, wake up those that have no scheduled state message, so that the state machine is run through
	for(std::map<int,std::tuple<int,int,simtime_t,simtime_t>>::iterator it = activeContactData_.begin(); it != activeContactData_.end(); it++ )
	{
		int cid = it->first;
		if(!sdr_.isBundleForContact(cid))
			routing->refreshForwarding(contactPlan_.getContactById(cid));

		if(stateMsgs_.count(cid) == 0)
		{
			//no state message scheduled, schedule one now
			StateMsg *stateMsg = new StateMsg("stateMsg", STATE_MACHINE);
			stateMsg->setSchedulingPriority(STATE_MACHINE);
			stateMsg->setContactId(cid);
			stateMsg->setBundleSentReport(false);
			//schedule
			scheduleAt(simTime(), stateMsg);
			//add to list
			stateMsgs_[cid] = stateMsg;
		}
	}
}

void Dtn::setOnFault(bool onFault)
{
	this->onFault = onFault;

	// Local and remote forwarding recovery
	if (onFault == false)
	{
		// Wake-up local un-scheduled forwarding threads
		this->refreshForwarding();

		// Wake-up remote un-scheduled forwarding threads
		for(std::map<int,std::tuple<int,int,simtime_t,simtime_t>>::iterator it = activeContactData_.begin(); it != activeContactData_.end(); it++ )
		{
			Dtn * remoteDtn = (Dtn *) this->getParentModule()->getParentModule()->getSubmodule("node", get<NEIGHBOR_EID_INDEX>(it->second))->getSubmodule("dtn");
			remoteDtn->refreshForwarding();
		}
	}
}

ContactPlan* Dtn::getContactPlanPointer(void)
{
	return &this->contactPlan_;
}

Routing * Dtn::getRouting(void)
{
	return this->routing;
}

void Dtn::setWaitForCOM(bool setting)
{
	waitForCom = setting;
}

/**
 * Implementation of method inherited from observer to update gui according to the number of
 * bundles stored in sdr.
 */
void Dtn::update(void)
{
	// update srd size text
	graphicsModule->setBundlesInSdr(sdr_.getBundlesCountInSdr());
}

