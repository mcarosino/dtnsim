/*
 * TimeSlotPlan.cc
 *
 *  Created on: Feb 12, 2018
 *      Author: micha
 */
#include <com/TimeSlotPlan.h>

TimeSlotPlan::~TimeSlotPlan()
{

}

TimeSlotPlan::TimeSlotPlan()
{

}

//   Timeslot plan
//0) Group Start time t_s
//1) Group End time t_e
//1) My first Slot time in Group t_f
//2) My slot width t_w
//3) Time allocated to the other nodes in the grouping t_r
//4) numNodes in Group N (not using for now)
//5) My Order # (not using for now))
//6) This contact's destination/next hop node ID #

void TimeSlotPlan::parseTimeSlotPlanFile(string fileName)
{
	int id = 1;
	double start = 0.0;
	double end = 0.0;
	double first = 0.0;
	double width = 0.0;
	double remain = 0.0;
	int numNodes = 0;
	int orderNum = 0;
	int eid = 0;
	int nextHop = 0;

	string fileLine = "#";
	string a;
	string command;
	ifstream file;

	file.open(fileName.c_str());

	if (!file.is_open())
		throw cException(("Error: wrong path to timeslot file " + string(fileName)).c_str());

	while (getline(file, fileLine))
	{
		if (fileLine.empty())
			continue;

		if (fileLine.at(0) == '#')
			continue;

		// This seems to be a valid command line, parse it
		stringstream stringLine(fileLine);
		stringLine >> eid >> start >> end >> first >> width >> remain >> numNodes >> orderNum >> nextHop;
		this->addGroup(eid,start,end,first,width,remain,numNodes,orderNum,nextHop);

	}

	if (cin.bad())
	{
		// IO error
	}
	else if (!cin.eof())
	{
		// format error (not possible with getline but possible with operator>>)
	}
	else
	{
		// format error (not possible with getline but possible with operator>>)
		// or end of file (can't make the difference)
	}

	file.close();

}

//searches thru the list to find all groups that include node with given eid number, returns a vector of all the groups for that eid.
std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> TimeSlotPlan::getPlanForNode(int eid)
{
	std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> tmp;
	std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int> tmp2;
	for(std::vector<std::tuple<int,simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>>::iterator it = timeSlotPlan_.begin(); it != timeSlotPlan_.end(); it++ )
	{
		if(get<0>(*it) == eid)
		{
			tmp2 = std::make_tuple(get<1>(*it),get<2>(*it),get<3>(*it),get<4>(*it),get<5>(*it),get<6>(*it),get<7>(*it),get<8>(*it));
			tmp.push_back(tmp2);
		}
	}
	return tmp;
}

//add a group to the bulk list - (note these will be searched thru by the getPlanForNode function and distributed to the various nodes)
void TimeSlotPlan::addGroup(int eid, double start, double end, double first, double width, double remain, int numNodes, int orderNum, int nextHop)
{
	std::tuple<int,simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int> tmp;
	tmp = std::make_tuple(eid,simtime_t(start),simtime_t(end),simtime_t(first),simtime_t(width),simtime_t(remain),numNodes,orderNum,nextHop);
	timeSlotPlan_.push_back(tmp);
}

