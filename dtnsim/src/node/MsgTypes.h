#ifndef SRC_NODE_MSGTYPES_H_
#define SRC_NODE_MSGTYPES_H_

// Messages types and their priority are
// equal, unique and defined in this file.
// Higher scheduling priority (smaller
// numeric value) are executed first.

#define CONTACT_END_TIMER 0
#define CONTACT_START_TIMER 1

#define TIMESLOT_END_TIMER 2
#define TIMESLOT_START_TIMER 3

#define COM_QUERY_TO_DTN 4

#define STATE_MACHINE 5

#define TRAFFIC_TIMER 6

#define BUNDLE_CUSTODY_REPORT 7 // white
#define BUNDLE 8				// yellow

#define CUSTODY_TIMEOUT 9

#define FORWARDING_MSG_END 10
#define FORWARDING_MSG_START 11

#define FAULT_END_TIMER 21
#define FAULT_START_TIMER 20

#endif /* SRC_NODE_MSGTYPES_H_ */
