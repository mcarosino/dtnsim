#!/bin/bash

# create a new results folder
DATE=$(date +%d-%m-%Y_""%H:%M:%S)
mkdir ../results/$DATE
mkdir ../results/$DATE/data
mkdir ../results/$DATE/figures
#copy sim data over
cp ../../useCases/tdma_testing/*.ini ../results/$DATE/
cp ../../useCases/tdma_testing/results/* ../results/$DATE/data/


#generate new pdf files from .vec and .sca files
python mainVec.py ../../useCases/tdma_testing/results ../results/$DATE/figures "General-#0.vec"
python mainSca.py ../../useCases/tdma_testing/results ../results/$DATE/figures "General-#0.sca"
