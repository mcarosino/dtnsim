function x = find_first_empty_slot(group_list)
    x = 0;
    for i =1:length(group_list(:,1))
        if isempty(group_list{i,1})
            x = i;
            break
        end
    end
end