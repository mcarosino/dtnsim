#include <com/mac/MACTDMACentralized.h>

//<slotGroupStartTime,slot
MACTDMACentralized::MACTDMACentralized(int eid, int nodeNum, double guardBandPercent, std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>> &timeSlotPlan)
	: MAC()
{
	nodeNum_ = nodeNum;
	eid_ = eid;
	timeSlotPlan_ = timeSlotPlan;
	guardBandPercent_ = guardBandPercent;



}

MACTDMACentralized::~MACTDMACentralized() {

}


//   Timeslot plan
//0) Group Start time t_s
//1) Group End time t_e
//1) My first Slot time in Group t_f
//2) My slot width t_w
//3) Time allocated to the other nodes in the grouping t_r
//4) numNodes in Group N (not using for now)
//5) My Order # (not using for now)


//NOTE: When a node doesn't get allocated a timeslot in a given group, the timeslot plan should have been set (Before being read into the sim) as t_f > t_e


//Searches through the various groups trying to find the first one where our node has a timeslot to use. Note, that the timeslot may be shorter than expected - in which case we truncate it.
//e.g. if we are the last slot in the local group, all the other slots may have been 2 sec long but ours may only be 1 sec long, so we should check the min(expectedSlotEnd,localGroupEnd) to be sure
simtime_t MACTDMACentralized::getFirstSlotTime() {
	simtime_t grpBeginTime;
	simtime_t grpEndTime;
	simtime_t firstSlotTime;
	currentIndex_ = 0;
	bool foundfirst = false;

	for(std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>>::iterator it = timeSlotPlan_.begin(); it != timeSlotPlan_.end(); it++ )
	{
		grpBeginTime = get<GROUP_START>(*it);
		firstSlotTime = get<FIRST_SLOT>(*it);
		grpEndTime = get<GROUP_END>(*it);


		//Verify we have a slot
		if(firstSlotTime < grpEndTime)
		{
			//slot found, save some information and break to do additional computations
			fullSlotWidth_ = get<SLOT_WIDTH>(*it);
			otherNodeSlotWidths_ = get<REMAIN_WIDTH>(*it);
			currentSlotNumber_ = 1;
			currentState_ = (*it);
			foundfirst = true;
			break;

		}
		currentIndex_++;

	}

	if(foundfirst == false)
	{
		//this node has no timeslots!
		currentSlotStart_ = SIMTIME_MAX;
		return currentSlotStart_;
	}
	//Verify if the slot will be full or truncated
	if( firstSlotTime + fullSlotWidth_ > grpEndTime)
	{
		//Our slot will be smaller than expected, truncate to end time
		fullSlotWidth_ = grpEndTime - firstSlotTime;
	}
	currentSlotStart_ = firstSlotTime;

	//Compute the actual transmit window after considering guardband percentage
	transmitSlotWidth_ = fullSlotWidth_*(1-guardBandPercent_);

	//Compute the actual valid slot end of transmission time (i.e. reduced by guardband width)
	currentSlotEnd_ = currentSlotStart_ + transmitSlotWidth_;

	return currentSlotStart_;
}

//Return end of the current slot - should ONLY be called while we are inside a valid timeslot! Behavior when called outside of a valid timeslot will be wrong!
simtime_t MACTDMACentralized::getSlotTimeEnd() {
	//TODO: checks to warn if called outside of a valid slot
	return currentSlotEnd_;
}

int MACTDMACentralized::getNextHopNodeID() {
	return get<NEXT_HOP>(currentState_);
}

//Return the start of the next slot, regardless of if we are currently in a valid slot or not!
simtime_t MACTDMACentralized::getNextSlotTime() {
	//If we've ran out of slots dont bother recomputing any of this stuff
	if(currentSlotStart_ == SIMTIME_MAX)
		return SIMTIME_MAX;
	//Run the update state function with force = 1, telling it to move to the next state even if we are currently in a valid slot
	updateNextStateAndSlot(1);
	return currentSlotStart_;


}

//Return either the current time, if we are in a valid timeslot, or the next timeslot start time if we are not in the valid transmit portion of a slot.
simtime_t MACTDMACentralized::getNextTransmitTime() {
	//If we've ran out of slots dont bother recomputing any of this stuff
	if(currentSlotStart_ == SIMTIME_MAX)
		return SIMTIME_MAX;

	//First check that we are in the correct group and update currentslotsart/currentslotend
	//Called with force = 0 as we only want to update the state if we are outside a valid slot.
	updateNextStateAndSlot(0);

	if( simTime() >= currentSlotStart_ && simTime() < currentSlotEnd_)
	{
		//we are inside of the current slot, return time now
		return simTime();
	}

	//otherwise, we need to return the transmit slot start time in the future
	return currentSlotStart_;


}

//force = 0 -> only verify we are in correct group/slot and update as needed if we aren't
//force = 1 -> force move to next slot
void MACTDMACentralized::updateNextStateAndSlot(int force)
{
	simtime_t currentTime = simTime();
	simtime_t grpBeginTime;
	simtime_t grpEndTime;
	grpEndTime = get<GROUP_END>(timeSlotPlan_.at(currentIndex_));

	if(force == 0)
	{	//when force = 0, if the current time indicates that we are currently in or before the currentSlotEnd, we are in the correct state already and should break out.
		if(currentTime < currentSlotEnd_)
			return;
	}

	//We are not in the current slot period (or force == 1), see if we are still in current group
	if(currentTime < grpEndTime)
	{
		// We are still inside the correct group, see if we have another timeslot in this group

		//loop using slot number to find the next slot
		while(1)
		{
			currentSlotNumber_++;
			//The kth (k=1,2,3,...) slot start time is t_f + (k-1)*(t_w + t_r)
			//The kth slot end time (this is raw end time, e.g. including guardband will result in earlier end time) is t_f + k*t_w + (k-1)*t_r
			currentSlotEnd_ = get<FIRST_SLOT>(currentState_) + currentSlotNumber_*get<SLOT_WIDTH>(currentState_) + (currentSlotNumber_ - 1)*get<REMAIN_WIDTH>(currentState_);
			fullSlotWidth_ = get<SLOT_WIDTH>(currentState_);
			currentSlotStart_ = currentSlotEnd_ - fullSlotWidth_;
			//first verify that the slot is in the group, if not, break out so we can find the next group
			if(currentSlotStart_ >= grpEndTime)
				break;
			//Next see if the current time is before the (raw) end of this slot
			if(currentTime < currentSlotEnd_)
			{

				//check if slot will need to be truncated
				if(currentSlotEnd_ > grpEndTime)
					fullSlotWidth_ = grpEndTime - currentSlotStart_;
				//Compute the actual transmit time after considering guardband
				transmitSlotWidth_ = fullSlotWidth_*(1.0-guardBandPercent_);
				//actual slot end time(this is the last time we can transmit before the guardband starts)
				currentSlotEnd_ = currentSlotStart_ + transmitSlotWidth_;

				//One final check that current time is not in the guardband
				if(currentTime < currentSlotEnd_)
					return;
			}
		}
	}

	//If we get here, We are either past the current group or don't have another timeslot in it, thus we need to search for the next group
	//and return its first slot assuming that the simTime <= timeslot_end


	currentIndex_++;
	//looping over the remaining groups
	for(std::vector<std::tuple<simtime_t,simtime_t,simtime_t,simtime_t,simtime_t,int,int,int>>::iterator it = (timeSlotPlan_.begin() + currentIndex_); it != timeSlotPlan_.end(); it++ )
	{
		grpBeginTime = get<GROUP_START>(*it);
		currentSlotStart_ = get<FIRST_SLOT>(*it);
		currentSlotNumber_ = 1;
		grpEndTime = get<GROUP_END>(*it);

		//looping over all my slots in the group
		while(1)
		{
			currentSlotEnd_ = get<FIRST_SLOT>(*it) + currentSlotNumber_*get<SLOT_WIDTH>(*it) + (currentSlotNumber_ - 1)*get<REMAIN_WIDTH>(*it);
			fullSlotWidth_ = get<SLOT_WIDTH>(*it);
			currentSlotStart_ = currentSlotEnd_ - fullSlotWidth_;
			//first verify that the slot is in the group, if not, break out so we can find the next group
			if(currentSlotStart_ >= grpEndTime)
				break;
			currentState_ = (*it);
			//Next see if the current time is before the (raw) end of this slot
			if(currentTime < currentSlotEnd_)
			{
				//check if slot will need to be truncated
				if(currentSlotEnd_ > grpEndTime)
					fullSlotWidth_ = grpEndTime - currentSlotStart_;
				//Compute the actual transmit time after considering guardband
				transmitSlotWidth_ = fullSlotWidth_*(1.0-guardBandPercent_);
				//actual slot end time(this is the last time we can transmit before the guardband starts)
				currentSlotEnd_ = currentSlotStart_ + transmitSlotWidth_;

				//Okay we have found a timeslot in this group and computed its parameters, but now we need to make sure its not already passed given the new guardband computation
				if(currentTime < currentSlotEnd_)
				{
					//Timeslot has not passed, but we might already be inside of it, check that and re-compute the start if so
					if(currentTime > currentSlotStart_)
					{
						//TODO: Is this correct to re-compute the guardband portion if we start transmitting in a timeslot part of the way through?
						fullSlotWidth_ = currentSlotStart_ + fullSlotWidth_ - currentTime;
						transmitSlotWidth_ = fullSlotWidth_*(1.0 - guardBandPercent_);
						currentSlotStart_ = currentTime;
						currentSlotEnd_ = currentSlotStart_ + transmitSlotWidth_;
					}
					return;
				}
			}
			//increment slot number to check next slot in the group
			currentSlotNumber_++;
		}
	}


	//If we get to this line, it means we found no more valid slots, set variables to dummy values to prevent any sends
	//No more bundles will be transmitted in the simulation for this node.
	currentSlotStart_ = SIMTIME_MAX;
	return;

}


